import './App.css';
import CommunitySection from './Components/CommunitySection/CommunitySection';
import JoinUsSection from './Components/JoinUsSection/JoinUsSection';

function App() {
  return (
    <div className="App">
      <CommunitySection/>
      <JoinUsSection/>
    </div>
  );
}

export default App;
