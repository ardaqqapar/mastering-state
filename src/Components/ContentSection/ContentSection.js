import s from './ContentSection.module.css'
import Card from '../Card/Card';

const ContentSection = ({people}) => {
    return (
        <>
            <p className={s.subtitle}>We’re proud of our products, and we’re really excited when we get feedback from our users.</p>
            <div className={s.list}>
                {people.map((person)=>(
                    <Card key={person.id} person={person}/>
                ))}
            </div>
        </>
    )
}

export default ContentSection;