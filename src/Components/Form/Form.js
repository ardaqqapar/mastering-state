import { useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import { subscribe, unsubscribe } from '../../reduxToolkit/operations';
import { selectIsSubscribed, selectLoading, selectError } from '../../selectors'

import s from './Form.module.css'

const Form = () => {
    const [email, setEmail] = useState('');
    const isSubscribed = useSelector(selectIsSubscribed);
    const loading = useSelector(selectLoading);
    const error = useSelector(selectError);
    const dispatch = useDispatch();
    
    const handleSubmit = (e) => {
        e.preventDefault();
        if (isSubscribed) {
          dispatch(unsubscribe(email))
            .then(() => {
              setEmail('');
            })
        } else {
          dispatch(subscribe(email))
          .then(()=>{
            if(error) {
                window.alert(error)
            }
          })
          
        }
      };

    return (
        <form className={s.form} onSubmit={handleSubmit}>
            {!isSubscribed && (
                <input
                    className={s.input}
                    type="email"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    disabled={loading}
                />
            )}
            <button style={{ opacity: loading ? 0.5 : 1 }} className={s.button} type="submit" disabled={loading}>
                {isSubscribed ? (loading ? 'Unsubscribing...' : 'Unsubscribe') : (loading ? 'Subscribing...' : 'Subscribe')}
            </button>
        </form>
    )
}

export default Form