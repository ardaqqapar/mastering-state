import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  visible: true,
};

const contentSlice = createSlice({
  name: 'content',
  initialState,
  reducers: {
    toggleVisibility: (state) => {
      state.visible = !state.visible;
    },
  },
});

export const { toggleVisibility } = contentSlice.actions;

export default contentSlice.reducer;