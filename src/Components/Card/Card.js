import s from './Card.module.css'

const Card = ({ person }) => {
    return(
        <div className={s.card}>
            <img src={person.avatar}/>
            <h2>{person.firstName}</h2>
            <p>{person.position}</p>
        </div>
    )
}
export default Card;