import { createSlice } from '@reduxjs/toolkit';
import { subscribe, unsubscribe } from './operations';

const initialState = {
  email: '',
  subscribed: false,
  loading: false,
  error: null,
};

const subscriptionSlice = createSlice({
  name: 'subscription',
  initialState,
  reducers: {
    setEmail: (state, action) => {
      state.email = action.payload;
    },
    setSubscribed: (state, action) => {
      state.subscribed = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(subscribe.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(subscribe.fulfilled, (state) => {
        state.loading = false;
        state.subscribed = true;
      })
      .addCase(subscribe.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      })
      .addCase(unsubscribe.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(unsubscribe.fulfilled, (state) => {
        state.loading = false;
        state.subscribed = false;
        state.email = '';
      })
      .addCase(unsubscribe.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message;
      });
  },
});

export const { setEmail, setSubscribed } = subscriptionSlice.actions;

export default subscriptionSlice.reducer;
