import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const fetchPeople = createAsyncThunk('people/fetch', async () => {
    const response = await axios.get('http://localhost:3000/community');
    return response.data;
});

export const subscribe = createAsyncThunk(
    'subscription/subscribe',
    async (email) => {
      const response = await axios.post('http://localhost:3000/subscribe', { email });
      if (response.status === 200) {
        return email;
      } else {
        throw new Error(response.data.error);
      }
    }
  );

export const unsubscribe = createAsyncThunk('subscription/unsubscribe', async (email) => {
  const response = await axios.post('http://localhost:3000/unsubscribe', { email });
  return response.data;
});